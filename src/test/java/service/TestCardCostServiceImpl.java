package service;

import dto.CardCostDTO;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class TestCardCostServiceImpl {

    @Test()
    public void validateCardCost_CaseNullCountry() {
        CardCostServiceImpl sub = new CardCostServiceImpl();
        CardCostDTO cardCostDTO = new CardCostDTO();
        cardCostDTO.setCost(3);
        cardCostDTO.setCountry("");
        boolean thrown = false;
        String actualMessage = null;

        try {
            sub.validateCardCost(cardCostDTO);
        } catch (RuntimeException e) {
            thrown = true;
            actualMessage = e.getMessage();
        }

        String expectedMessage = "The fields cost and country cannot be empty";
        assertTrue(thrown);
        assertEquals(actualMessage,expectedMessage);
    }

    @Test()
    public void validateCardCost_CaseNullCost() {
        CardCostServiceImpl sub = new CardCostServiceImpl();
        CardCostDTO cardCostDTO = new CardCostDTO();
        cardCostDTO.setCountry("DEN");
        boolean thrown = false;
        String actualMessage = null;

        try {
            sub.validateCardCost(cardCostDTO);
        } catch (RuntimeException e) {
            thrown = true;
            actualMessage = e.getMessage();
        }

        String expectedMessage = "The fields cost and country cannot be empty";
        assertTrue(thrown);
        assertEquals(actualMessage,expectedMessage);
    }

    @Test()
    public void validateCardCost_CaseCardNumberVerySmall() {
        CardCostServiceImpl sub = new CardCostServiceImpl();
        CardCostDTO cardCostDTO = new CardCostDTO();
        cardCostDTO.setCardNumber("1235");

        boolean thrown = false;
        String actualMessage = null;

        try {
            sub.validateCardCost(cardCostDTO);
        } catch (RuntimeException e) {
            thrown = true;
            actualMessage = e.getMessage();
        }

        String expectedMessage = "The card number must be 8 to 19 digits";
        assertTrue(thrown);
        assertEquals(actualMessage,expectedMessage);
    }

    @Test()
    public void validateCardCost_CaseCountryAlreadyExists() {
        CardCostServiceImpl sub = mock(CardCostServiceImpl.class);
        List<CardCostDTO> cardCosts = new LinkedList<>();
        CardCostDTO cardCostDTO = new CardCostDTO();
        cardCostDTO.setCost(3);
        cardCostDTO.setCountry("DEN");
        CardCostDTO cardCostDTO1 = new CardCostDTO();
        cardCostDTO1.setCountry("GR");
        cardCostDTO1.setCost(15);
        CardCostDTO cardCostDTO2 = new CardCostDTO();
        cardCostDTO2.setCountry("den");
        cardCostDTO2.setCost(10);

        cardCosts.add(cardCostDTO1);
        cardCosts.add(cardCostDTO2);

        boolean thrown = false;
        String actualMessage = null;

        when(sub.getAllCardCosts()).thenReturn(cardCosts);
        doCallRealMethod().when(sub).validateCardCost(any(CardCostDTO.class));

        try {
            sub.validateCardCost(cardCostDTO);
        } catch (RuntimeException e) {
            thrown = true;
            actualMessage = e.getMessage();
        }

        String expectedMessage = "The cost of this  country already exists";
        assertTrue(thrown);
        assertEquals(expectedMessage,actualMessage);
    }

    @Test()
    public void validateCardCost_CaseCompleted() {
        CardCostServiceImpl sub = mock(CardCostServiceImpl.class);
        List<CardCostDTO> cardCosts = new LinkedList<>();
        CardCostDTO cardCostDTO = new CardCostDTO();
        cardCostDTO.setCost(3);
        cardCostDTO.setCountry("DEN");
        CardCostDTO cardCostDTO1 = new CardCostDTO();
        cardCostDTO1.setCountry("GR");
        cardCostDTO1.setCost(15);
        CardCostDTO cardCostDTO2 = new CardCostDTO();
        cardCostDTO2.setCountry("MLT");
        cardCostDTO2.setCost(10);

        cardCosts.add(cardCostDTO1);
        cardCosts.add(cardCostDTO2);

        boolean thrown = false;
        String actualMessage = null;

        when(sub.getAllCardCosts()).thenReturn(cardCosts);
        doCallRealMethod().when(sub).validateCardCost(any(CardCostDTO.class));

        try {
            sub.validateCardCost(cardCostDTO);
        } catch (RuntimeException e) {
            thrown = true;
            actualMessage = e.getMessage();
        }

        assertFalse(thrown);
    }



}
