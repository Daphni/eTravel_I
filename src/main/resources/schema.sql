CREATE TABLE CardCost (
    Id int NOT NULL  PRIMARY KEY,
    Country varchar(10) NOT NULL UNIQUE,
    Cost int NOT NULL
);