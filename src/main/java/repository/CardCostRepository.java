package repository;

import domain.CardCost;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CardCostRepository extends JpaRepository<CardCost, Long> {

    List<CardCost> findAll();

    Optional<CardCost> findById(Long id);

    List<CardCost> findByCountry(String country);

    CardCost save(CardCost cardCost);

    void deleteById(Long id);

    void deleteByCountry(String country);

}
