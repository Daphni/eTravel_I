package domain;

import javax.persistence.*;

@Entity
@Table(name = "CardCost")
public class CardCost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name="Country", nullable = false, unique=true)
    private String country;

    @Column(name="Cost", nullable = false)
    private Integer cost;

    @Transient
    private String CardNumber;

    public CardCost(Long id, String country, Integer cost) {
        this.id = id;
        this.country = country;
        this.cost = cost;
    }

    public CardCost() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getCost() {
        return cost;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String cardNumber) {
        CardNumber = cardNumber;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }
}
