package Controller;

import converter.CardCostConverter;
import domain.CardCost;
import dto.CardCostDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.CardCostService;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CardCostController {

    @Autowired
    private CardCostService cardCostService;

    @Autowired
    private CardCostConverter cardCostConverter;



    @GetMapping("/movies")
    ResponseEntity<List<CardCostDTO>> getCardCost() {
        List<CardCostDTO> cardCosts= cardCostService.getAllCardCosts();
        return ResponseEntity.status(HttpStatus.OK).body(cardCosts);

    }

    @GetMapping("/{id}")
    public ResponseEntity<CardCostDTO> findById(@PathVariable("id") Long id) {
        CardCostDTO cardCostDTO = cardCostConverter.convertCardCost(cardCostService.findCardCostByID(id));

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(cardCostDTO);
    }

    @PostMapping("/cardCosts")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CardCostDTO> createCardCost (@RequestBody CardCostDTO cardCostDTO){
        CardCostDTO saveCardCost = cardCostService.addCardCost(cardCostDTO);
        return new ResponseEntity<>(saveCardCost, HttpStatus.CREATED);

    }
    @PostMapping("/cardCosts/delete")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity deleteCardCost (@RequestBody CardCostDTO cardCostDTO){

        cardCostService.deleteById(cardCostDTO.getId());
        return  ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }




}
