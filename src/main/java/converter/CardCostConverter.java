package converter;

import domain.CardCost;
import dto.CardCostDTO;

public class CardCostConverter {

    public CardCostDTO convertCardCost(CardCost cardCost) {
        CardCostDTO cardCostDTO = new CardCostDTO();
        cardCostDTO.setId(cardCost.getId());
        cardCostDTO.setCost(cardCost.getCost());
        cardCostDTO.setCardNumber(cardCost.getCardNumber());
        cardCostDTO.setCountry(cardCost.getCountry());
        return cardCostDTO;
    }

    public CardCost convertCardCost(CardCostDTO cardCostDTO){
        CardCost cardCost = new CardCost();
        cardCost.setId(cardCostDTO.getId());
        cardCost.setCost(cardCostDTO.getCost());
        cardCost.setCardNumber(cardCostDTO.getCardNumber());
        cardCost.setCountry(cardCostDTO.getCountry());

        return cardCost;
    }

}
