package service;

import domain.CardCost;
import dto.CardCostDTO;

import java.util.List;

public interface CardCostService {
    CardCost findCardCostByID(Long id);

    List<CardCostDTO> getAllCardCosts();

    CardCostDTO addCardCost(CardCostDTO cardCostDTO);

    void deleteById(Long id);

}
