package service;

import converter.CardCostConverter;
import domain.CardCost;
import dto.CardCostDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import repository.CardCostRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardCostServiceImpl implements CardCostService{


    @Autowired
    private CardCostRepository cardCostRepository;

    @Autowired
    private CardCostConverter cardCostConverter;


    @Override
    public CardCost findCardCostByID(Long id) {
        return cardCostRepository.findById(id).get();
    }

    @Override
    public List<CardCostDTO> getAllCardCosts() {
        return convertDomainToDTO(cardCostRepository.findAll());
    }

    @Override
    public CardCostDTO addCardCost(CardCostDTO cardCostDTO) {
        validateCardCost(cardCostDTO);
        if(cardCostDTO.getCardNumber()!=null) {
            String num = cardCostDTO.getCardNumber().trim().substring(0,6);
//            call https://binlist.net/ + num;
//            from response :
//            cardCostDTO.setCountry(response.getCountry());
//            cardCostDTO.setCost(response.getCost());
        }
        CardCost cardCost = cardCostRepository.save(cardCostConverter.convertCardCost(cardCostDTO));
        return cardCostConverter.convertCardCost(cardCost);

    }

    @Override
    public void deleteById(Long id)  {
        cardCostRepository.deleteById(id);
    }


    private List<CardCostDTO> convertDomainToDTO(List<CardCost> cardCosts) {
        return cardCosts
                .stream()
                .map(cardCostConverter::convertCardCost)
                .collect(Collectors.toList());
    }

     void validateCardCost(CardCostDTO cardCostDTO) {
        boolean hasCardNumber = cardCostDTO.getCardNumber()!=null;
        if (hasCardNumber) {
           Integer digits = cardCostDTO.getCardNumber().replace(" ","").length();
           if(digits<6|| digits>19){
               throw new RuntimeException( "The card number must be 8 to 19 digits");
           }
        } else {
            if(StringUtils.isEmpty(cardCostDTO.getCountry()) || cardCostDTO.getCost()==null) {
                throw new RuntimeException( "The fields cost and country cannot be empty");
            }
            List<CardCostDTO> cardCosts = getAllCardCosts();
            List<String> countries =  cardCosts.stream().map(CardCostDTO::getCountry).collect(Collectors.toList());
            if(containsIgnoreCase(countries,cardCostDTO.getCountry())) {
                throw new RuntimeException( "The cost of this  country already exists");
            }
        }

    }
    public static boolean containsIgnoreCase(Collection<String> list, String str) {
        if (list != null && !list.isEmpty()) {
            for (String s : new ArrayList<>(list)) {
                if (s != null && s.equalsIgnoreCase(str)) {
                    return true;
                }
            }
        }
        return false;
    }

}
